# Information-theoretic Analysis of Motor Control

This project contains the code used to run an experiment where 36 human participants tried to balance simulated pendulums of different lengths. The experiments and the obtained results are reported in [Volpi et al. 2022]

- [ ] The simulation of the pendulums has been performed using the code stored in the directory "Pendulum Simulator". See the enclosed ReadMe.txt file and the comments within the code to get more details.
- [ ] The information-theoretic analysis of the collected data has been performed using the code stored in the directory "Data Analysis". See the enclosed ReadMe.txt file and the comments within the code to get more details.


[Volpi et al. 2022] Nicola Catenacci Volpi, Martin Greaves, Dari Trendafilov, Christoph Salge, Giovanni Pezzulo, Daniel Polani, Skilled motor control implies a low entropy of states but a high entropy of actions. arXiv: 2112.11637, 2021.


