% -- INFO_DATA_ANALYSYS.m -- 
%
% Copyright 2022 Nicola Catenacci Volpi, The University Of Hertfordshire.
% License included in the current directory.
%
%   This MATLAB script acnalyzes the data collected during an experiment
%   where 36 human participants tried to balance a simulated inverted pendulum. 
%   The information-theoretic analysis is performed using the numerical
%   estimator MIToolbox v2.0 (https://mloss.org/revision/view/1602/) 
%   [Brown et al. 2012] located in the folder "MIToolbox".

%   INPUT: pendulum and participants data stored in the folder "data", with 
%   files named "FRI_p_t", where "p" stands for participant_id and "t" stands 
%   for pendulum length. 
% 
%   OUTPUT: "Routput" files with stored all trials' pendulum lengths, utilities 
%   ("reward"), entropies of state H(S) ("HS"), conditional entropies of state 
%   given action H(A|S) ("HAS"), conditional entropies of next state given 
%   action H(S'|A) ("HSsA") and mutual information between action and next 
%   state I(A;S') ("IASs"). These information-theoretic quantities are plotted 
%   with respect to pendulum length and utility. Furthermore, the mean number 
%   of key presses in/out the balancing region and proportion of time spent 
%   there per pendulum length are also plotted. These plots correspond to 
%   Figures 2, 3, 5 and 6 of our paper [Volpi et al. 2022].
%
%
%
%  [Brown et al. 2012]  Gavin Brown, Adam Pocock, Ming-Jie Zhao, and Mikel 
%  Luja�n. Conditional likelihood maximisation: a unifying framework for 
%  information theoretic feature selection. The journal of machine learning 
%  research, 13(1):27�66, 2012.
%
%  [Volpi et al. 2022] Nicola Catenacci Volpi, Martin Greaves, Dari Trendafilov, 
%  Christoph Salge, Giovanni Pezzulo, Daniel Polani, Skilled motor control 
%  implies a low entropy of states but a high entropy of actions. 
%  arXiv: 2112.11637, 2021.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% add path where the MIToolbox library is located
Current_Folder = pwd;
MIToolbox_directory = strcat(pwd,'/MIToolbox');
addpath(MIToolbox_directory);

% 3*N arrays storing output data with all pendulum lengths and utilities 
% together with H(S), H(A|S), H(S'|A) or I(A;S')
output_HS= []; 
output_HAS= [];
output_HSsA= [];
output_IASs= []; 

% arrays storing all utilities, H(S), H(A|S), H(S'|A) and I(A;S') used for
% plots
Vreward = [];
VHS = [];
VHAS = [];
VHSsA = [];
VIs = [];

% array storing the colors assigned to each data point in the plots 
C = [];

% counter of key presses per pendulum length and in/out balancing region
count = zeros(2,8);
% counter of steps in balancing region per pendulum length
count_bal = zeros(1,8);

c = 0;
% for each pendulum's length...
for jj=2:9
    
    % data point's color corresponding to this pendulum's length
    c=c+1;
  
  % for each participant...  
  for ii=1:36
            disp("processing data of participant " + num2str(ii) + " and pendulum length " + num2str(jj) + "...");
            
            % load data of trial in "DATA" data structure
            sii = num2str(ii);
            sjj = num2str(jj);
            filename = strcat('data/FRI_', sii,'_', sjj);          
            DATA = dlmread(filename, ' ');
            
            % trials' utility values are stored in DATA first entry
            reward = DATA(1,1);
            DATA = DATA(2:7027, :);
            
            DATA = DATA';
            
            % load participants' action values
            action = DATA(1, :);
            % load pendulum's angles
            pos = DATA(3, :);
            dim = size(pos);
            length = dim(2);
            % load pendulum's angular velocities
            vel = DATA(2, :);
            
            % shift pendulum's angular velocities toward positive values
            % and scale it for future binning
            b_vel = (vel + 10.0)*10;
            % bin pendulum's angles
            b_pos = floor(pos*1000)/1000;
            
            % store in "state" an unique natural number combining angle and angular
            % velocity (indirectly this also completes their binning)
            b_pos = b_pos * 1000000;
            state = floor(b_pos + b_vel);
            
            action = action';
            state = state';
            
            % shift states of 270ms to generate "next state" S' random variable 
            % and store it in "sstate"
            sstate = state(16:end);
            % actions dimension matched with "sstate" dimension to have sound 
            % mutual information computation  
            actio = action(1:end-15);
            
            
            %%%%%%%%%%%%% COMPUTE INFORMATION-THEORETIC MEASURES %%%%%%%%%
            
            
            % compute H(S)
            HS = h(state);
            % compute H(A|S)
            HAS = condh(action, state);
            % compute H(S'|A)
            HSsA = condh(sstate, actio);
            % compute I(A;S')
            Is = mi(actio,sstate);


            Vreward = [Vreward , reward];
            VHS = [VHS, HS];
            VHAS = [VHAS, HAS];
            VHSsA = [VHSsA, HSsA];
            VIs = [VIs, Is];
            
            row_HS = [jj, reward, HS];
            output_HS = [output_HS; row_HS];
            row_HAS = [jj, reward, HAS];
            output_HAS = [output_HAS; row_HAS];
            row_HSsA = [jj, reward, HSsA];
            output_HSsA = [output_HSsA; row_HSsA];
            row_IASs = [jj, reward, Is];
            output_IASs = [output_IASs; row_IASs];
            
            C = [C, c];
            
            
            
            %%%%%%%%%%%%%%%% BALANCING REGION ANALYSIS %%%%%%%%%%%%%%%
            % absolute value of the distance from balance angle
            norm_pos = 0;
            % flag indicating whether participant is pressing a key
            pressed = 0;
            % flag indicating whether pendulum is in balancing region
            bal = 0;
            
            % absolute value of the balancing region's boundary (rad)
            boundary = 0.2735;
            
            % for all data points...
            for j=1:length
                
                
                
                % compute absolute value of the distance from balance angle
                if pos(j) > 3.1415 && pos(j) < 6.2831 
                   norm_pos = 6.2831 - pos(j);
                end
                
                % if pendulum is in the balancing region...
                if norm_pos < boundary 
                    bal = 1;
                    % update counter of steps in balancing region per pendulum length
                    count_bal(jj-1) = count_bal(jj-1) +1;
                else
                    bal = 2;
                end
                
                % if participant is pressing a key...
                if action(j) ~= 0 && pressed == 0
                    % update counter of key presses per pendulum length and in/out balancing region
                    count(bal,(jj-1)) = count(bal,(jj-1)) + 1;
                    pressed = 1;
                    
                end
                
                
                if action(j) == 0 && pressed == 1

                    pressed = 0;
                    
                end
                    
            end
          
      end
end

% save files containing pendulum lengths, utilities together with H(S), 
% H(A|S), H(S'|A) I(A;S') to disk
dlmwrite('Routput_HS', output_HS,';');
dlmwrite('Routput_HAS', output_HAS,';');
dlmwrite('Routput_HSsA', output_HSsA,';');
dlmwrite('Routput_IASs', output_IASs,';');

% compute mean number of key presses in/out balancing region and proprtion of
% time spent there per pendulum length
tot = length*36;
for g=1:8
    count(1,g) = count(1,g)/36.0;
    count(2,g) = count(2,g)/36.0;
    count_bal(g) = count_bal(g) / tot; 
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PLOTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% scatter plot H(S) VS Utility
figure
gscatter(VHS, Vreward,C);
xlabel('H(S)');
ylabel('Utility');

% scatter plot H(A|S) VS Utility
figure
gscatter(VHAS, Vreward,C);
xlabel('H(A|S)');
ylabel('Utility');

% scatter plot H(S'|A) VS Utility
figure
gscatter(VHSsA, Vreward,C);
xlabel('H(S''16|A)');
ylabel('Utility');

% scatter plot I(A;S') VS Utility
figure
gscatter(VIs, Vreward,C);
xlabel('I(A;S''16)');
ylabel('Utility');


% plot mean number of key presses VS pendulum length
figure 
plot(count(1,:))
hold on 
plot(count(2,:))
legend({'in balance region','out balance region'},'Location','northwest')
set(gca, 'XTickLabel', [0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9]);
xlabel('L  (m)')
ylabel('Mean Number of Key Presses')
axis square

% plot proportion of time in balancing region VS pendulum length
figure
plot(count_bal)
set(gca, 'XTickLabel', [0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9]);
xlabel('L  (m)')
ylabel('Proportion of Time in Balance Region')
axis square

