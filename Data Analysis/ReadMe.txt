-- INFO_DATA_ANALYSYS.m --


This MATLAB script acnalyzes the data collected during an experiment
where 36 human participants tried to balance a simulated inverted pendulum. 
The information-theoretic analysis is performed using the numerical
estimator MIToolbox v2.0 (https://mloss.org/revision/view/1602/) 
[Brown et al. 2012] located in the folder "MIToolbox".
 
INPUT: pendulum and participants data stored in the folder "data", with 
files named "FRI_p_t", where "p" stands for participant_id and "t" stands 
for pendulum length. 
 
OUTPUT: "Routput" files with stored all trials' pendulum lengths, utilities 
("reward"), entropies of state H(S) ("HS"), conditional entropies of action 
given state H(A|S) ("HAS"), conditional entropies of next state given 
action H(S'|A) ("HSsA") and mutual information between action and next 
state I(A;S') ("IASs"). These information-theoretic quantities are plotted 
with respect to pendulum length and utility. Furthermore, the mean number 
of key presses in/out the balancing region and proportion of time spent 
there per pendulum length are also plotted. These plots correspond to 
Figures 2, 3, 5 and 6 of our paper [Volpi et al. 2022].



[Brown et al. 2012]  Gavin Brown, Adam Pocock, Ming-Jie Zhao, and Mikel 
Luja\'b4n. Conditional likelihood maximisation: a unifying framework for 
information theoretic feature selection. The journal of machine learning 
research, 13(1):27\'9666, 2012.

[Volpi et al. 2022] Nicola Catenacci Volpi, Martin Greaves, Dari Trendafilov, 
Christoph Salge, Giovanni Pezzulo, Daniel Polani, Skilled motor control 
implies a low entropy of states but a high entropy of actions. 
arXiv: 2112.11637, 2021.