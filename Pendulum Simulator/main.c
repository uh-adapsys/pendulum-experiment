/* ****************************************************************************************************************************
-- MAIN.C --
  
Copyright 2022 Christoph Salge, The University Of Hertfordshire.
License included in the project directory.
  
This C++ code simulates the dynamics of pendulums of different lengths and
displays their motion on the screen. The pendulums can be controlled by users
using the keys of the keyboard. The simulation has been used to collect data of
the experiment reported in [Volpi et al. 2022], where 36 human participants
tried to balance the pendulums.
This code uses ad requires the graphics library OpenGL (https://www.opengl.org//).

Runs trials for single participant logging data to .csv file in working directory.
File name is of structure CL1_n.csv where n represents the participant number
 assigned at the beginning of the experiment.
The data written to the file takes the following form:

<subject number>,<trial number>,<pendulum length>, <event number>,<event code>,<time>,<pendulum angle>,<pendulum angular velocity>

where:

Subject number is the number assigned to the participant at the start of the experiment

Trial number is the number of the current trial

Pendulum length

Event number is a count that starts from 1 and increases by 1 each time a new event is recorded.

Event code is a code that identifies the particular event.  The codes are as follows:
99  Start of trial
-99 End of trial
1   Clockwise key pressed ('Z' key)
-1  Clockwise key released
2   Anti-clockwise key pressed ('M' key)
-2  Anti-clockwise key released ('M' key)
9   Pendulum within balance angle
-9  Pendulum no longer within balance angle
0   Status update

Time represents the elapsed time since the start of the trial

Pendulum angle represents the current pendulum angle as plotted on screen

Pendulum angular velocity represents the current angular velocity of the pendulum

 
 [Volpi et al. 2022] Nicola Catenacci Volpi, Martin Greaves, Dari Trendafilov,
 Christoph Salge, Giovanni Pezzulo, Daniel Polani, Skilled motor control implies
 a low entropy of states but a high entropy of actions. arXiv: 2112.11637, 2021.

 **************************************************************************************************************************** */




#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glut.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <Eigen/Core>

using namespace std;
using namespace Eigen;

#include "pendulumworld.h"

#define PI 3.14159



//**************************************************

//Initial parameter declarations


//global variables

double controlInput;     //indicates active key press
bool Pleft;              //Pendulum control key press
bool Pright;             //Pendulum control key press
bool Sp;                 //Space bar inout control
int kpresscode;          //defines key press action
float StartTime;         //Time at start of trial



//Screen parameters and messages
std::string datafilename;
std::string scrnmsg;
std::string demotrialmsg = ("DEMONSTRATION TRIALS");
std::string practrialmsg = ("PRACTICE TRIALS");
std::string exptrialmsg = ("EXPERIMENTAL TRIALS");
std::string keypressmsg = "PRESS SPACE BAR TO CONTINUE";
std::string scrnstartmsg = ("TRIAL ");
std::string scrnendmsg = ("END OF TRIAL");
std::string breakmsg = ("PLEASE TAKE A TWO MINUTE BREAK");
std::string endtrialmsg = ("CALL EXPERIMENTER TO CONTINUE");
float scrnpos;
float demomsgpos = -0.30;
float pracmsgpos = -0.20;
float expmsgpos = -0.27;
float endtrialpos = -0.37;

int scrn_msg_time = 3000;     //Initial screen message time
int start_msg_time = 4000;    //Start of trial screen messaage display time in ms
int end_msg_time = 3000;      //End of trial screen messaage display time in ms

int xdim = 700;       //Size of screen
int ydim = 700;       //Size of screen




//experimental data structure
struct trial
{
    int Ppt, Ntrial[50], Ncond[4];   //trial number condition number (if relevent else = 1;
    float Lpend[50];                 //pendulum length
    int NCount[10000], NEvent[10000];
    float NTime[10000], NAngle[10000], NAngVel[10000], NElapTime[10000];
};

/* ** Trial durations ** */
int demotrialdur = 60;   //duration of demo trial
int practrialdur = 180;   //duration of practice trial
int expttrialdur = 120;   //duration of experimental trial


/*  ** Temp values **   */
//int demotrialdur = 10;   //duration of demo trial
//int practrialdur = 20;   //duration of demo trial
//int expttrialdur = 20;   //duration of demo trial


int ITI = 5;             //Inter trial interval
int ncond = 2;           //Number of conditions
int npractice = 3;       //number of practice trials
int nexpt = 5;           //number of exptl trials
int trial_num;           //number of trials
float plength;           //current pendulum length
float AngThresh = 20;    //Balance threshold - angular distance in degrees from vertical



/*  ** Pendulum lengths for trials **  */
float demolength[] = {0.8, 0.4};                                              //demonstration trials
float practlength[] = {0.8, 0.7, 0.5, 0.3};                                   //practice trial lengths
float exptlength[] = {0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};                //experimental trial lengths



/*  ** Temp values **   */
//float demolength[] = {0.9};             //demonstration trials
//float practlength[] = {0.9, 0.2};                                   //practice trial lengths
//float exptlength[] = {0.9, 0.2, 0.6, 0.4};                //experimental trial lengths



/* Event log message codes */
int Starttrial = 99;   //Start of trial code
int Endtrial = -99;    //End of trial code
int CwKeyPress = 1;    //Clockwise key pressed ('Z' key)
int CwKeyRel = -1;     //Clockwise key released ('Z' key)
int AcwKeyPress = 2;   //Anti-clockwise key pressed  ('M' key)
int AcwKeyRel = -2;    //Anti-clockwise key released ('M' key)
int Pbal = 9;          //Pendulum within balance angle
int Punbal = -9;       //Pendulum no longer within balance angle
int Update = 0;        //Status update





//**************************************************

LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
void EnableOpenGL(HWND hwnd, HDC*, HGLRC*);
void DisableOpenGL(HWND, HDC, HGLRC);


//**************************************************

void reshape(float w, float h)
{
    GLfloat width, height;

    glViewport (0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if (w > h)
    {
        width = w/h;
        height = 1;
    }
    else
    {
        width = 1;
        height = h/w;
    }
    glOrtho(-width, width, -height, height, -1.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}


//**************************************************

float elapse_t (float stime, float ctime)
{
    float etime;
    etime = ctime - stime;  //calculate elasped time
    return (etime);
}


//**************************************************

float cur_pos (float angl)
{
    int nrev;
    float cpos;
    nrev = int(angl/(2 * PI));  //calculate number of complete revolutions of pendulum
    cpos = angl - (nrev * 2 * PI);
    return (cpos);
}


//**************************************************

void update_struct (trial& subj, int dcount, int event, float ptime, float angl, float angl_speed)
{
    subj.NCount[dcount] = dcount;
    subj.NEvent[dcount] = event;
    subj.NTime[dcount] = elapse_t(StartTime, ptime);
    subj.NAngle[dcount] = angl;   // if require 0-2*Pi range, then use: cur_pos(pangle);
    subj.NAngVel[dcount] = angl_speed;
}


//**************************************************

bool chk_bal (float c_ang)
{
    float p_ang, thresh, llim, ulim;

    if (c_ang < 0) p_ang = 0 - c_ang;  //current angle is negaative convert it into positive
    else p_ang = c_ang;

    thresh = AngThresh * PI / 180;
    llim = PI - thresh;  //calculate upper threshold value in radians
    ulim = PI + thresh;  //calculate lower threshold value in radians

    if ((p_ang > llim) && (p_ang < ulim))  // test if balanced at current angle
        return(1);
    else
        return(0);
}


 //**************************************************

void press_key(MSG& msg, unsigned int keycode)
{
    bool fdone = false;
    while (!fdone)
    {
        PeekMessage(&msg, NULL, 0, 0, PM_REMOVE);
        switch (msg.message)
        {
            case WM_KEYDOWN:
                if (msg.wParam == keycode)
                    fdone = true;
                break;
        }
    }
}


 //**************************************************

void write_data(ofstream& opdatafile, trial& subj, int dcount, int tnum)
{
   int d;
   for (d=1; d<=dcount; d++)
   opdatafile << subj.Ppt << "," << subj.Ntrial[tnum] << "," << subj.Lpend[tnum] << "," << subj.NCount[d] << "," << subj.NEvent[d] << "," << subj.NTime[d] << "," << subj.NAngle[d] << "," << subj.NAngVel[d] << endl;
}


//**************************************************
//             END OF FUNCTIONS
//**************************************************





int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine,
                   int nCmdShow)
{
    WNDCLASSEX wcex;
    HWND hwnd;
    HDC hDC;
    HGLRC hRC;
    MSG msg;
    BOOL bQuit = FALSE;


    /* initialize random seed: */
    srand (time(NULL));


    // EXPERIMENTAL PARAMETER DEFINITIONS

    //create trial data structure

    trial subject;

    //declare function variables
    int trialrun = 0;        //Trial in progress flag
    float pangle = 0;        //Current angle of pendulum
    float pangle_speed = 0;  //Current angular velocity of pendulum
    float CurTime;           //Current time at point of event
    float updtime;           //Current time at status update
    int kpress = 0;          //indicates a control key pressed
    bool prev_bal = 0;       //indicates previous state of pendulum - whether balanced or not
    bool cur_bal = 0;        //indicates whether pendulum within balance threshold;
    float sbal_time;         //start time when pendulum initially triggers balance threshold
    float cbal_time;         //current time for which pendulum balanced

    int trialdur;
    int expt=0;                //practice trials or experimental trials
    int ntrials;             //total number of trials
    int pcount;
    int demoarrsize = sizeof(demolength)/sizeof(demolength[0]);      //number of demonstration trials
    int practarrsize = sizeof(practlength)/sizeof(practlength[0]);   //number of practice trials
    int exptarrsize = sizeof(exptlength)/sizeof(exptlength[0]);      //number of experimental trials
    int code;                                                        //key press ASCII value

    ArrayXf TestArray;                                               //holds pendulum lengths for current trials



    /* register window class */
    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_OWNDC;
    wcex.lpfnWndProc = WindowProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = "Pendulum";
    wcex.hIconSm = LoadIcon(NULL, IDI_APPLICATION);;


    if (!RegisterClassEx(&wcex))
        return 0;


    ostringstream convert;  //stream for number to character conversion

    //obtain subject number and create data file
    cout << "Subject number:" <<endl;
    cin >> subject.Ppt;
    cout << "Subject number is " << subject.Ppt << endl;
    convert << subject.Ppt;
    datafilename = "RI1_"+ convert.str() + ".csv";
    cout << datafilename << endl;

    ofstream opfile;
    opfile.open (datafilename.c_str(), ios::out | ios::app);



    /* create main window */
    hwnd = CreateWindowEx(0,
                          "Pendulum",
                          "Pendulum study",
                          WS_OVERLAPPEDWINDOW,
                          100,            //CW_USEDEFAULT,
                          50,             //CW_USEDEFAULT,
                          xdim,
                          ydim,
                          NULL,
                          NULL,
                          hInstance,
                          NULL);


    int datacount;

    /* enable OpenGL for the window */
    EnableOpenGL(hwnd, &hDC, &hRC);


    nCmdShow = 3;
    Pright = false;
    Pleft = false;



    // ******* START DISPLAY WINDOW ********

    ShowWindow(hwnd, nCmdShow);
    SetForegroundWindow(hwnd);

    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    SwapBuffers(hDC);

    for (expt=0; expt<=2; expt++)

    {
        switch (expt)
        {
        case 0:
            ntrials = demoarrsize;
            TestArray.resize(ntrials);
            for (pcount=0; pcount<ntrials; pcount++)
                {
                    TestArray(pcount)=demolength[pcount];
                }

            scrnmsg = demotrialmsg;
            scrnpos = demomsgpos;
            trialdur = demotrialdur;
            break;

        case 1:
            ntrials = practarrsize;
            TestArray.resize(ntrials);
            for (pcount=0; pcount<ntrials; pcount++)
                {
                    TestArray(pcount)=practlength[pcount];
                }

            scrnmsg = practrialmsg;
            scrnpos = pracmsgpos;
            trialdur = practrialdur;
            break;

        case 2:
            ntrials = exptarrsize;
            TestArray.resize(ntrials);

            random_shuffle(&exptlength[0], &exptlength[ntrials]);
            for (pcount=0; pcount<ntrials; pcount++)
                {
                    TestArray(pcount)=exptlength[pcount];
                }

            scrnmsg = exptrialmsg;
            scrnpos = expmsgpos;
            trialdur = expttrialdur;
            break;
        }


        cout << "Number of trials: " << ntrials << endl;


        //Initialize everything neede for the program here:
        PendulumWorld world =  PendulumWorld(0.0,0.0,0.0,0.0);
        world.clearWindow();
        SwapBuffers(hDC);
        Sleep (1000);

        //Displau trial messages
        cout << scrnmsg << endl;

        world.screen_msg(scrnpos, 0.0, scrnmsg);       //Display start of trial message on screen
        world.screen_msg(endtrialpos, -0.30, keypressmsg);    //Display press spacebar to continue message on screen
        SwapBuffers(hDC);
        //PeekMessage(&msg, NULL, 0, 0, PM_REMOVE);


        code = (0x20);                              //space bar ASCII code
        press_key(msg, code);                       //Test for space bar press
        world.clearWindow();
        SwapBuffers(hDC);
        Sleep (2000);

        // *********  Start current trial  ********

        for (trial_num = 1; trial_num <= ntrials; trial_num++)

        {
        cout << "Starting trial " << trial_num << endl;

            kpress = 0;
            controlInput = 0;
            datacount = 0;
            plength = TestArray[trial_num-1];
            bQuit = FALSE;
            cur_bal = 0;

            if (trial_num > 1)
                Sleep (ITI*1000);

            cout << "Pendulum length " << plength << endl;


            //Initialize everything neede for the program here:
            PendulumWorld world =  PendulumWorld(0.1,0.0,plength,0.3);


            world.clearWindow();
            SwapBuffers(hDC);



            subject.Ntrial[trial_num] = trial_num;
            subject.Lpend[trial_num] = plength;
            convert.str("");                    //Clear string stream
            convert << subject.Ntrial[trial_num];
            scrnmsg = scrnstartmsg+convert.str();
            cout << scrnmsg << endl;
            world.screen_msg(-0.10, 0.0, scrnmsg);       //Display start of trial message on screen
            SwapBuffers(hDC);
            Sleep (start_msg_time);

            world.clearWindow();
            SwapBuffers(hDC);
            Sleep (1000);


            StartTime = clock()/1000.0;
            trialrun = 1;    //Trial started

            //Record start time in data file
            datacount++;
            update_struct (subject, datacount, Starttrial, StartTime, pangle, pangle_speed);  //Start of trial event code
            printf ("%4d  %2d  %5.3f  %5.2f, %5.2f\n",subject.NCount[datacount], subject.NEvent[datacount], subject.NTime[datacount], subject.NAngle[datacount], subject.NAngVel[datacount]);


            //Datalogging default parameters


            /* program main loop */
            while (!bQuit)
            {

                       /* check for messages */
                if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
                {
                    /* handle or dispatch messages */
                    if (msg.message == WM_QUIT)
                    {
                        bQuit = TRUE;

                    }
                    else
                    {
                        TranslateMessage(&msg);
                        DispatchMessage(&msg);
                    }
                }
                else

                    // terminate trial when time limit is exceeded
                {
                    CurTime = clock()/1000.0;


                    /*  END OF TRIAL - WHEN ELAPSED TIME > TRIAL DURATION */
                    if (elapse_t(StartTime, CurTime) > trialdur)

                    {
                        /* If end of trial and data not written to file write data to file */

                        if (trialrun == 1)
                        {
                            /* If pendulum still balanced at end of trial write end of balance to data - code commented out as unnecessary... */
                      /*
                            if (cur_bal == 1)
                            {
                                datacount++;
                                cbal_time = clock()/1000.0;
                                update_struct (subject, datacount, Punbal, cbal_time, pangle, pangle_speed);  //End of trial, indicates end of balance event code
                                cout << "  Balance time: " << elapse_t(sbal_time, cbal_time) << endl;
                                printf ("%4d  %2d  %5.3f  %5.2f, %5.2f\n",subject.NCount[datacount], subject.NEvent[datacount], subject.NTime[datacount], subject.NAngle[datacount], subject.NAngVel[datacount]);
                            }
                        */

                            /* output end of trial log into data (code = -99) */
                            datacount++;
                            CurTime = clock()/1000.0;
                            update_struct (subject, datacount, Endtrial, CurTime, pangle, pangle_speed);      //End of trial event code
                            printf ("%4d  %2d  %5.3f  %5.2f, %5.2f\n",subject.NCount[datacount], subject.NEvent[datacount], subject.NTime[datacount], subject.NAngle[datacount], subject.NAngVel[datacount]);

                            if (expt==2)
                                write_data(opfile, subject, datacount, trial_num);      //write trial data to data file

                            trialrun = 0;                                         //clear trial running flag - trial ended
                        }


                        /* If half-way through experimental trials take a break.
                        If final trial of set of trials then terminate trials accordingly */

                        if ((trial_num == ntrials) | ((expt ==2) && (trial_num == ntrials/2)))
                        {
                            world.clearWindow();                //Clear pendulum

                            //mid-experiment 2 minute break
                            if ((expt ==2) && (trial_num == ntrials/2))
                            {
                                world.screen_msg(endtrialpos, 0.0, breakmsg);       //Display Please take a break message on screen
                                SwapBuffers(hDC);
                                Sleep(120000);
                                //Sleep(1);
                                world.clearWindow();
                                world.screen_msg(endtrialpos, 0.0, keypressmsg);       //Display press spacebar to continue message on screen
                                code = (0x20);
                                SwapBuffers(hDC);
                                press_key(msg, code);
                                world.clearWindow();
                                SwapBuffers(hDC);                                  //Test for key press - space bar or 'c'
                                break;
                            }

                            else
                            //End of current phase (Practice trials or Experimental trials)
                            {
                            if (expt==0)
                            {
                                world.screen_msg(endtrialpos, 0.0, keypressmsg);       //Display press spacebar to continue message on screen
                                code = (0x20);
                            }
                            else
                            {
                                world.screen_msg(-0.40, 0.0, endtrialmsg);    //Display call experimenter to continue message on screen
                                code = (0x43);
                            }

                            SwapBuffers(hDC);

                            press_key(msg, code);                                   //Test for key press - space bar or 'c'
                            world.clearWindow();
                            SwapBuffers(hDC);
                            Sleep (2000);
                            break;
                            }
                        }

                        else     //if end of trial but not end of set of trials
                        {
                            world.clearWindow();                                    //Clear pendulum
                            world.screen_msg(-0.17, 0.0, scrnendmsg);               //Display end of trial message on screen
                            SwapBuffers(hDC);
                            Sleep (end_msg_time);                                   //Message display time
                        }


                        if ((!Pleft) && (!Pright))
                        {
                            bQuit = TRUE;
                            world.clearWindow();
                            SwapBuffers(hDC);
                       }

                    }

                    else
                    {


                    //update event structure for change in keypress status

                    if (Pleft | Pright)
                    {
                    if (kpress==0)
                        {
                            datacount++;
                            update_struct (subject, datacount, kpresscode, CurTime, pangle, pangle_speed);      // log key press event code to data
                            kpress = 1;
                            printf ("%4d  %2d  %5.3f  %5.2f, %5.2f\n",subject.NCount[datacount], subject.NEvent[datacount], subject.NTime[datacount], subject.NAngle[datacount], subject.NAngVel[datacount]);
                        }
                    }

                    if (!Pleft && !Pright)
                    {
                        if (kpress == 1)
                        {
                            datacount++;
                            update_struct (subject, datacount, kpresscode, CurTime, pangle, pangle_speed);      // log key release event code to data
                            kpress = 0;
                            printf ("%4d  %2d  %5.3f  %5.2f, %5.2f\n",subject.NCount[datacount], subject.NEvent[datacount], subject.NTime[datacount], subject.NAngle[datacount], subject.NAngVel[datacount]);
                        }
                    }



                    glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
                    glClear(GL_COLOR_BUFFER_BIT);

                    for (int i = 0; i<100 ; i++)
                    {
                        world.tick(controlInput,0.0003);
                        pangle = world.angle;
                        pangle_speed = world.angleSpeed;


                        //test if pendulum is balanced

                        prev_bal = cur_bal;
                        cur_bal = chk_bal(cur_pos(pangle));


                        //if change in balance status, update event structure

                        if (cur_bal != prev_bal)
                        {
                        datacount++;

                            if(cur_bal==1)
                            {
                                sbal_time = clock()/1000.0;
                                update_struct (subject, datacount, Pbal, sbal_time, pangle, pangle_speed);       // pendulum balanced code
                                cout << "  Start time: "<< elapse_t(StartTime, sbal_time) << endl;
                            }
                            else
                            {
                                cbal_time = clock()/1000.0;
                                update_struct (subject, datacount, Punbal, cbal_time, pangle, pangle_speed);     //pendulum not balanced code
                                cout << "  Balance time: " << elapse_t(sbal_time, cbal_time) << endl;
                            }
                            prev_bal = cur_bal;

                            printf ("%4d  %2d  %5.3f  %5.2f, %5.2f\n",subject.NCount[datacount], subject.NEvent[datacount], subject.NTime[datacount], subject.NAngle[datacount], subject.NAngVel[datacount]);

                        }
                    }

                    //record status update in event log

                    datacount++;
                    updtime = clock()/1000.0;
                    update_struct (subject, datacount, Update, updtime, pangle, pangle_speed);     //pendulum not balanced code

                    //enable code below if required for debugging - disabled in normal use
               //     printf ("%4d  %2d  %5.3f  %5.2f, %5.2f\n",subject.NCount[datacount], subject.NEvent[datacount], subject.NTime[datacount], subject.NAngle[datacount], subject.NAngVel[datacount]);



                    //Draw updated pendulum position
                    world.drawPendulum();

                    SwapBuffers(hDC);
                    Sleep (10);
                }
                }
            }
        }
    }


    //*****  Terminate experiment *****

    //Close output data file
    opfile.close();

    /* shutdown OpenGL */
    DisableOpenGL(hwnd, hDC, hRC);

    /* destroy the window explicitly */
    DestroyWindow(hwnd);

    //Pause until spacebar pressed
    cin.ignore();
    printf ("Press return to continue\n");
    Sleep (50);
    cin.get();


return msg.wParam;
}



//**************************************************

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    int width, height;

    switch (uMsg)
    {
        case WM_CLOSE:
            PostQuitMessage(0);
        break;

        case WM_DESTROY:
            return 0;

        case WM_KEYDOWN:
        {
            switch (wParam)
            {
                case VK_ESCAPE:
                    PostQuitMessage(0);
                break;
                case (0x4D):
                    Pleft = true;
                    controlInput = 1;            //pendulum direction code (anti-clockwise)
                    kpresscode = AcwKeyPress;    //key code (M pressed) for datafile

                break;
                case (0x5A):
                    Pright = true;
                    controlInput = -1;           //pendulum direction code (clockwise)
                    kpresscode = CwKeyPress;     //key code (Z pressed) for datafile
                break;
            }
        }
        break;
        case WM_KEYUP:
        {
            switch (wParam)
            {
                case (0x4D):
                    Pleft = false;
                    controlInput = 0;
                    kpresscode = AcwKeyRel;     //key code (M released) for datafile

                break;
                 case (0x5A):
                    Pright = false;
                    controlInput = 0;
                    kpresscode = CwKeyRel;      //key code (Z released) for datafile

                break;
                case (0x20):
                    Sp = false;
                break;
            }
        }
        break;
        case WM_SIZE:
        {
            height = HIWORD(lParam);
            width = LOWORD(lParam);
            reshape(width, height);
            return 0;
        }
        break;



        default:
            return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }

    return 0;
}


//**************************************************

void EnableOpenGL(HWND hwnd, HDC* hDC, HGLRC* hRC)
{
    PIXELFORMATDESCRIPTOR pfd;

    int iFormat;

    /* get the device context (DC) */
    *hDC = GetDC(hwnd);

    /* set the pixel format for the DC */
    ZeroMemory(&pfd, sizeof(pfd));

    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW |
                  PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.iLayerType = PFD_MAIN_PLANE;

    iFormat = ChoosePixelFormat(*hDC, &pfd);

    SetPixelFormat(*hDC, iFormat, &pfd);

    /* create and enable the render context (RC) */
    *hRC = wglCreateContext(*hDC);

    wglMakeCurrent(*hDC, *hRC);
}



//**************************************************
void DisableOpenGL (HWND hwnd, HDC hDC, HGLRC hRC)
{
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(hRC);
    ReleaseDC(hwnd, hDC);
}
