#ifndef PENDULUMWORLD_H
#define PENDULUMWORLD_H

#include <math.h>

//damping of the pendulum
#define DAMPING 0.000005
# define PI 3.14159


class PendulumWorld
{
    public:

        float angle;
        float angleSpeed;
        float angleAcc;
        float length;
        float powerInc;
        float externalAcc;
        float pwrdir;
        float tlen = 0.27;  //Power direction arrow length

        PendulumWorld(float angleInput, float angleSpeedInput,float lengthInput,float powerIncInput = 1.0f)
        {
            angle = angleInput;
            angleSpeed = angleSpeedInput;
            length = lengthInput;
            powerInc = powerIncInput;
            angleAcc = 0.0f;
        }

        virtual ~PendulumWorld() {}



        void tick(double externalAccelleration = 0.0, double timestepLength = 0.5)
        {
            externalAcc = externalAccelleration * powerInc * (0.9 / length) ;
            float futureAngleAcc = (externalAcc - (sin( angle ) )/length );
            angle += angleSpeed * timestepLength;
            angleSpeed += (futureAngleAcc * timestepLength - angleSpeed * (DAMPING) );
        }



        void drawPendulum()
        {

            glPushMatrix();

            //indicates current control input = external acceleration
            //these arrows are placed in the middle
//            glBegin(GL_TRIANGLES);
//            glColor3f(1.0,0.3, 0.3f);
//
//            glVertex2f(0.0f, 0.0f);
//            glVertex2f(externalAcc, 0.025f);
//            glVertex2f(0.0f, 0.05f);
//            glEnd();
//

            //from here on, drawing the pendulum
            //this rotates the whole pendulumg according to its current angle
            glRotatef((angle*180.0)/PI, 0.0f, 0.0f, 1.0f);


            //evaluate direction of acceleration following key press
            if (externalAcc!=0)
                pwrdir = (externalAcc/abs(externalAcc))*tlen;
            else
                pwrdir = 0;



            //draw the pendulum shaft
            glBegin(GL_POLYGON);
                glColor3f(0.0f, 0.0f, 0.7f);
                glVertex2f(-0.01,   0.0);
                glVertex2f(0.01,   0.0);
                glVertex2f(0.01,  -length);
                glVertex2f(-0.01, -length);
            glEnd();

            //moves the drawing coordinates down to the weight.
            glTranslatef(0.0f, -length, 0.0f);


            glBegin(GL_TRIANGLES);
                glColor3f(1.0, 0.3, 0.3f);
                //glVertex2f(-externalAcc, -0.04);
                glVertex2f(-pwrdir, -0.04);
                glVertex2f(0.0, 0.0);
                //glVertex2f(-externalAcc, 0.04);
                glVertex2f(-pwrdir, 0.04);
            glEnd();


            //draws the pendulum weight.
            glColor3f(0.0f, 0.0f, 0.9f);

            GLUquadricObj *qobj = gluNewQuadric();
            gluSphere(qobj,0.05,15,15);


            glPopMatrix();

        }



        void clearWindow()
        {
            glPushMatrix();

            glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
            glClear(GL_COLOR_BUFFER_BIT);

            glPopMatrix();
        }



        void screen_msg (GLfloat xcoord = -0.1, GLfloat ycoord = 0.0, string textstring = "a")
        {
            glPushMatrix();
            void * font = GLUT_BITMAP_TIMES_ROMAN_24;
            glColor3f(0.0f, 0.0f, 0.0f);
            glRasterPos2f(xcoord, ycoord);
            int len = textstring.length();
            for (int icount = 0; icount < len; icount++)
            {
                glutBitmapCharacter(font, textstring[icount]);
            }
        glPopMatrix();
        }



    protected:
    private:
};

#endif // PENDULUMWORLD_H
