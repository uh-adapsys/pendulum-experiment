Copyright 2022 Christoph Salge, The University Of Hertfordshire.
License included in the project directory.
  
This C++ code simulates the dynamics of pendulums of different lengths and
displays their motion on the screen. The pendulums can be controlled by users
using the keys of the keyboard. The simulation has been used to collect data of
the experiment reported in [Volpi et al. 2022], where 36 human participants
tried to balance the pendulums.
This code uses ad requires the graphics library OpenGL (https://www.opengl.org//).

Runs trials for single participant logging data to .csv file in working directory.
File name is of structure CL1_n.csv where n represents the participant number assigned at the beginning of the experiment.
The data written to the file takes the following form:

<subject number>,<trial number>,<pendulum length>, <event number>,<event code>,<time>,<pendulum angle>,<pendulum angular velocity>

where:

Subject number is the number assigned to the participant at the start of the experiment

Trial number is the number of the current trial

Pendulum length

Event number is a count that starts from 1 and increases by 1 each time a new event is recorded.

Event code is a code that identifies the particular event.  The codes are as follows:
99  Start of trial
-99 End of trial
1   Clockwise key pressed ('Z' key)
-1  Clockwise key released
2   Anti-clockwise key pressed ('M' key)
-2  Anti-clockwise key released ('M' key)
9   Pendulum within balance angle
-9  Pendulum no longer within balance angle
0   Status update

Time represents the elapsed time since the start of the trial

Pendulum angle represents the current pendulum angle as plotted on screen

Pendulum angular velocity represents the current angular velocity of the pendulum

 
 [Volpi et al. 2022] Nicola Catenacci Volpi, Martin Greaves, Dari Trendafilov,
 Christoph Salge, Giovanni Pezzulo, Daniel Polani, Skilled motor control implies
 a low entropy of states but a high entropy of actions. arXiv: 2112.11637, 2021.